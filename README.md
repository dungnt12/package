# Setup project
1. Install package

Vào __package__ `npm init` và cài

```js
npm install --save-dev typescript @babel/core @babel/cli @babel/plugin-proposal-class-properties @babel/preset-env @babel/preset-typescript
```

2. Add script to package.json

```js
"scripts": {
    "type-check": "tsc --noEmit",
    "type-check:watch": "npm run type-check -- --watch",
    "build": "npm run build:types && npm run build:js",
    "build:types": "tsc --emitDeclarationOnly",
    "build:js": "babel src --out-dir lib --extensions \".ts,.tsx\" --source-maps inline"
}
```

3. Custom tsconfig

```js
tsc --init --declaration --allowSyntheticDefaultImports --target esnext --outDir lib
```

---

## Setup npm publish
1. Init and login
```js
npm init
npm login
```

2. Setup
Trong package.json thêm ```"files": ["lib/**/*"]``` 
> Khi code xong sẽ build ra đây bằng `npm run build` nó chỉ lấy những file này đẩy lên npm

### Test
Có 2 dạng test


1. Tạo link ảo 
    - Project `__package__` gõ `npm link` sẽ tạo ra package có tên giống trong package.json phần tên
    - Vào `___test__` gõ `npm link [tên project phía trên]` rồi import như thường

2. Nhúng thằng link vào
    - Từ __test__ gọi thằng tới file của __package__

> Vì project tại __test__ đã được nhúng koa và next nên có thể tạo thẳng component trong đó để test
#### Tham khảo
https://github.com/microsoft/TypeScript-Babel-Starter

https://itnext.io/step-by-step-building-and-publishing-an-npm-typescript-package-44fe7164964c