import * as React from 'react';
// Backend test
import { get } from '../../__package__/lib';

function App() {
  React.useEffect(() => {
    get('https://jsonplaceholder.typicode.com/posts').then(({ data }) => console.log(data));
  }, []);

  return <>
    HELLO XO-team
  </>
}

export default App;